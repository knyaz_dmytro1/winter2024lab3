public class Panda{
	public int age;
	public double weight;
	public String name;
	public void isFat(){
		if (this.weight >= 100.0){
			System.out.println(this.name + " is " + this.weight + " kg heavy. He is a fat panda.");
		}
		else{
			System.out.println(name + " is " + this.weight + " kg heavy. He is a light panda.");
		}
	}
	public void presentHimself(){
		System.out.println("Hello! My name is " + this.name + " and I am " + this.age + " years old.");
	}
}